function search(){
	var searchStr = document.getElementById("recherche").value;
	var xhr = new XMLHttpRequest();
	var url = "http://localhost:1234/api?action=opensearch&search="+ searchStr + "&format=json";
	var responseStr = '';
	
	xhr.onreadystatechange=function(){
		if (xhr.readyState==4 && xhr.status==200){
			var responseParse = JSON.parse(xhr.responseText);
			for(i=0;i<responseParse[1].length;i++){
				responseStr += '<li>' + responseParse[1][i] + '</li>';
            }
			document.getElementById("res").innerHTML = responseStr;
		}
	}
	
	xhr.open("GET", url,true);
	xhr.send();
}